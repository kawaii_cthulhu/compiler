PROGRAM identifier; BEGIN END.

PROGRAM identifier; LABEL constant; BEGIN END.
PROGRAM identifier; LABEL constant(, constant)*; BEGIN END.

PROCEDURE identifier; BEGIN END;

PROCEDURE identifier; LABEL constant; BEGIN END;
PROCEDURE identifier; LABEL constant(, constant)*; BEGIN END;

PROCEDURE identifier(); BEGIN END ;

PROCEDURE identifier(); LABEL constant; BEGIN END ;
PROCEDURE identifier(); LABEL constant(, constant)*; BEGIN END;

1. Визначення головної програми та процедур без параметрів.
Дії:
- генерація заголовку для виклику програми чи процедури;
- генерація інструкцій повернення з підпрограми.

6. Оголошення міток
Дії:
- генерація таблиці міток;
- перетворення міток до вигляду ідентифікаторів для можливості вставки в асемблерний код;
Обмеження:
- не дозволяється використовувати однакові імена для двох і більше міток;
- імена згенерованих міток не мають збігатися з іменами змінних та констант в одній підпрограмі та з зовнішніми іменами на глобальному рівні.

8. Оператори
Дії:
- правилу <statements-list> --> <empty> відповідає інструкція nop;