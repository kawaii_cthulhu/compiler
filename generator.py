__author__ = 'kawaii_cthulhu'

from utils import *
from scanner import Scanner
from parser import Parser

import json


class CodeGenerator:
    def __init__(self, syntax_err, tree):
        self.tree = tree
        self.syntax_err = syntax_err
        self.is_program = True
        self.labels = None
        self.name = ''

    def run(self):
        if not self.syntax_err:
            if self.discover():
                return self.get_code()
            else:
                return ''


    def discover(self):
        if self.tree['<program>']['<program-program>']:
            self.is_program = True
            self.name = self.tree['<program>']['<program-program>']['<procedure-identifier>']['<identifier>']
        else:
            self.is_program = False
            self.name = self.tree['<program>']['<procedure-identifier>']['<identifier>']
        return self.check_labels()

    def check_labels(self):
        labels = self.get_labels()
        if not labels:
            return True
        checked = set()
        for label in labels:
            if label in checked:
                print 'There are two labels \'' + label + '\'.Label names should be different.'
                return False
            else:
                checked.add(label)
        return True

    def get_labels(self):
        result = []
        labels_tree = self.tree['<program>']
        if self.is_program:
            labels_tree = labels_tree['<program-program>']
        labels_tree = labels_tree['<block>']['<declarations>']['<label-declarations>']['<label-declaration>']
        if not labels_tree:
            return
        if labels_tree['<unsigned-integer>']['<digits>']:
            result.append(labels_tree['<unsigned-integer>']['<digits>'])
        while labels_tree:
            label_node = labels_tree['<labels-list>']['<labels-list-item>']
            if not label_node:
                break
            label = label_node['<unsigned-integer>']['<digits>']
            result.append(label)
            labels_tree = label_node
        self.labels = result
        return result

    def get_code(self):
        coded_labels = []
        if self.is_program:
            code = PROGRAM.format(self.name)
        else:
            code = PROCEDURE.format(self.name)
        #: resolve code template
        if not self.labels:
            self.labels = []
        else:
            for label in self.labels:
                coded_labels.append(LABEL.format(label))
        code = '\n'.join(coded_labels) + code
        return code


if __name__ == '__main__':
    attributes = dict_from_file('ascii.txt')
    keywords = dict_from_file('keywords.txt')

    scanner = Scanner(keywords, attributes)
    #program = 'examples/valid_program.txt'
    #program = 'examples/procedure.txt'
    #program = 'examples/comment.txt'
    #program = 'examples/invalid_char.txt'
    #program = 'examples/invalid_comment.txt'
    program = 'examples/comment2.txt'

    scanner_result = scanner.run(program)
    if scanner_result is None:
        exit()
    tokens, keywords, constants, identifiers = scanner_result
    keywords = reverse_dict(keywords)
    constants = reverse_dict(constants)
    identifiers = reverse_dict(identifiers)

    print tokens
    parser = Parser(tokens, keywords, constants, identifiers)

    result, tree = parser.start()
    #print json.dumps(tree, indent=4)
    generator = CodeGenerator(result, dicts(tree))
    print 'Code:\n'
    print generator.run()

    #print json.dumps(tree, indent=4)