__author__ = 'kawaii_cthulhu'

from collections import namedtuple, OrderedDict

from utils import *

Symbol = namedtuple('Symbol', ['value', 'attr'])


class Scanner:
    # TODO: initialization, tests
    """
    0 - whitespace
    1 - int start
    2 - identifier/keyword start
    3 - delimiter
    4 - comment start
    5 - err
    """
    def __init__(self, keywords, attributes):
        self.constants = OrderedDict()  # 501-1000
        self.identifiers = OrderedDict()  # 1001-
        self.keywords = keywords  # 401-500
        self.attributes = attributes  # ascii code: attribute
        self.program = ''
        self.line = 1
        self.tokens = []

    def gets(self, i):
        return Symbol(self.program[i],
                      self.attributes[str(ord(self.program[i]))])

    def const_tab_search(self, const):
        if const in self.constants.keys():
            return self.constants[const]

    def const_tab_form(self, const):
        try:
            self.constants[const] = self.constants.values()[-1] + 1
        except IndexError:
            self.constants[const] = 501
        return self.constants[const]

    def key_tab_search(self, key):
        if key in self.keywords.keys():
            return self.keywords[key]

    def idn_tab_search(self, idn):
        if idn in self.identifiers.keys():
            return self.identifiers[idn]

    def idn_tab_form(self, idn):
        try:
            self.identifiers[idn] = self.identifiers.values()[-1] + 1
        except IndexError:
            self.identifiers[idn] = 1001
        return self.identifiers[idn]

    def run(self, input_file):
        with open(input_file, 'r') as input:
            self.program = input.read()

        if self.program.strip() == '':
            print 'Empty file'
            return

        self.line = 0
        i = 0
        while i < len(self.program):
            symbol = self.gets(i)
            buf = ''
            lex_code = 0
            suppress_output = False
            # whitespace
            if symbol.attr == 0:
                #print 'whitespace'
                while i < len(self.program) and symbol.attr == 0:
                    i += 1
                    if ord(symbol.value) == 10:
                        self.line += 1
                    symbol = self.gets(i)
                suppress_output = True
            # constant
            elif symbol.attr == 1:
                #print 'const'
                while i < len(self.program) and symbol.attr == 1:
                    buf = buf + symbol.value
                    #print 'const buf: ' + buf
                    i += 1
                    symbol = self.gets(i)
                lex_code = self.const_tab_search(buf)
                if lex_code is None:
                    lex_code = self.const_tab_form(buf)
            # identifier
            elif symbol.attr == 2:
                #print 'id'
                while (i < len(self.program)) and (symbol.attr == 2
                                                   or symbol.attr == 1):
                    buf = buf + symbol.value
                    #print 'id buf: ' + buf
                    i += 1
                    symbol = self.gets(i)
                lex_code = self.key_tab_search(buf)
                if lex_code is None:
                    lex_code = self.idn_tab_search(buf)
                    if lex_code is None:
                        lex_code = self.idn_tab_form(buf)
            # comment
            elif symbol.attr == 3:
                if i == (len(self.program) - 1):
                    lex_code = ord('(')
                else:
                    i += 1
                    symbol = self.gets(i)
                    if symbol.value == '*':
                        if i == (len(self.program) - 1):
                            print '*) expected but end of file found'
                        else:
                            i += 1
                            symbol = self.gets(i)
                            while True:
                                while (i < (len(self.program) - 1) and
                                       symbol.value != '*'):
                                    i += 1
                                    symbol = self.gets(i)
                                if i == (len(self.program) - 1):
                                    print '*) expected but end of file found'
                                    if symbol.value == ')':
                                        symbol = Symbol(';', 4)
                                    break
                                else:
                                    i += 1
                                    symbol = self.gets(i)
                                if symbol.value == ')':
                                    break
                            if symbol.value == ')':
                                suppress_output = True
                            if i == (len(self.program) - 1):
                                lex_code = ord('(')
                    else:
                        lex_code = ord('(')
            # delimiter
            elif symbol.attr == 4:
                lex_code = ord(symbol.value)
                #print lex_code
                i += 1
            elif symbol.attr == 5:
                print 'Illegal symbol at line ' + str(self.line)
                return self.tokens, self.keywords, dict(self.constants), dict(self.identifiers)
            if not suppress_output:
                self.tokens.append(lex_code)
        if len(self.tokens) < 2:
            print 'Empty file'
            return
        return self.tokens, self.keywords, dict(self.constants), dict(self.identifiers)


if __name__ == '__main__':
    attributes = dict_from_file('ascii.txt')
    keywords = dict_from_file('keywords.txt')

    scanner = Scanner(keywords, attributes)
    scanner.run('examples/valid_program.txt')
    #scanner.run('examples/comment.txt')
    #scanner.run('examples/invalid_char.txt')
    #scanner.run('examples/invalid_comment.txt')
    #scanner.run('examples/comment2.txt')

    #print 'Scanner output:'
    #print scanner.tokens
    #print 'Constants:'
    #for const in scanner.constants:
    #    print const + ' ' + str(scanner.constants[const])
    #print 'Identifiers:'
    #for id in scanner.identifiers:
    #    print id + ' ' + str(scanner.identifiers[id])

