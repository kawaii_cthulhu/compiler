__author__ = 'kawaii_cthulhu'

from collections import OrderedDict, Callable


DELIMITERS = {';', '.', ',', '(', ')'}


AKM_TABLE = OrderedDict({'Start': (('SP', None, 'ERR'),
                                   ('#', 'OK', 'ERR')),
                         'SP': (('P', True, False),),
                         'P': (('PRG', True, None),
                               ('procedure', None, False),
                               ('PI', None, False),
                               ('PL', None, False),
                               (';', None, False),
                               ('B', None, False),
                               (';', True, False)),
                         'PRG': (('program', None, False),
                                 ('PI', None, False),
                                 (';', None, False),
                                 ('B', None, False),
                                 ('.', True, False)),
                         'B': (('D', None, False),
                               ('begin', None, False),
                               ('SL', None, False),
                               ('end', True, False)),
                         'D': (('LD', True, False),),
                         'LD': (('LB', True, None),
                                ('empty', True, False)),
                         'LB': (('label', None, False),
                                ('UI', None, False),
                                ('LL', None, False),
                                (';', True, False)),
                         'LL': (('ULL', True, None),
                                ('empty', True, False)),
                         'ULL': ((',', None, False),
                                 ('UI', None, False),
                                 ('LL', True, False)),
                         'PL': (('PD', True, None),
                                ('empty', True, False)),
                         'PD': (('(', None, False),
                                ('DL', None, False),
                                (')', True, False)),
                         'DL': (('empty', True, False),),
                         'SL': (('empty', True, False),),
                         'PI': (('identifier', True, False),),
                         'UI': (('constant', True, False),)})


BNF = {'SP': '<signal-program>', 'P': '<program>', 'Start': 'Start',
       'PRG': '<program-program>', 'B': '<block>',
       'D': '<declarations>', 'LD': '<label-declarations>',
       'LB': '<label-declaration>', 'LL': '<labels-list>',
       'ULL': '<labels-list-item>', 'PL': '<parameters-list>',
       'PD': '( <declarations-list> )', 'DL': '<declarations-list>',
       'SL': '<statements-list> ', 'PI': '<procedure-identifier>',
       'UI': '<unsigned-integer>', 'identifier': '<identifier>',
       'empty': '<empty>', 'constant': '<digits>'}

PROGRAM = """
.code
{0}:
nop;
END {0}
"""

PROCEDURE = """
{0}:
nop;
ret
"""

LABEL = 'a{0} LABEL UNKNOWN'


def dict_from_file(path):
    dict = {}
    with open(path, 'r') as f:
        item = f.readline().split()
        while item:
            dict[item[0]] = int(item[1])
            item = f.readline().split()
    return dict


def reverse_dict(dict):
    return {v: k for k, v in dict.items()}


def dicts(t):
    if 'dict' in t.__class__.__name__.lower():
        return {k: dicts(t[k]) for k in t}
    else:
        return t


class DefaultOrderedDict(OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and
           not isinstance(default_factory, Callable)):
            raise TypeError('first argument must be callable')
        OrderedDict.__init__(self, *a, **kw)
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return OrderedDict.__getitem__(self, key)
        except KeyError:
            return self.__missing__(key)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)


def tree():
    return DefaultOrderedDict(tree)


if __name__ == '__main__':
    pass