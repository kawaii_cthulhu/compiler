__author__ = 'kawaii_cthulhu'

from utils import *
from scanner import Scanner
import json


class Parser:
    def __init__(self, tokens, keywords, constants, identifiers):
        self.tokens = tokens
        self.keywords = keywords
        self.constants = constants
        self.identifiers = identifiers
        self.h = 0
        self.token = tokens[self.h]
        self.tree = tree()
        self.node = None
        self.err_h = 0
        #self.Output = ''

    def start(self):
        self.token = self.tokens[self.h]
        self.node = self.tree
        result = self.run_operation('Start') == 'OK'
        return not result, self.tree['Start']['<signal-program>']

    def err(self):
        if self.err_h < 0:
            return
        print 'Syntax error at ' + str(self.err_h) + ' at token ' + str(self.token)
        print 'Tree:'
        print json.dumps(self.tree, indent=4)

    def run_operation(self, operation):
        if self.h == len(self.tokens) and not operation == '#':
            self.err()
            return
        #print 'Run operation ' + operation

        bnf = False
        if operation in AKM_TABLE.keys():
            if bnf:
                self.node = self.node[operation]
            elif operation in BNF:
                self.node = self.node[BNF[operation]]
            node = self.node
        try:
            for command in AKM_TABLE[operation]:
                #print 'Run command ' + str(command)
                h = self.h
                self.node = node
                if self.run_operation(command[0]):
                    self.err_h = -1
                    #print str(command[0]) + ' returns true'
                    #self.Output += command[0] + ' '
                    #print command[0]
                    if not (command[1] is None):
                        if not bool(command[1]):
                            self.h = h
                        #print command[1]
                        return command[1]
                else:
                    #print str(command[0]) + ' returns false'
                    if self.err_h < 0:
                        self.err_h = self.h
                    if not (command[2] is None):
                        if command[2] == 'ERR':
                            self.err()
                            return
                        if not bool(command[2]):
                            self.h = h
                        #print command[2]
                        return command[2]
        except KeyError as e1:
            #print 'Operation key error ' + str(e1)
            if operation == '#' and self.h == len(self.tokens):
                self.node[operation] = '#'
                self.token = '#'
                return True
            if operation == 'empty':
                if bnf:
                    self.node[operation] = ''
                else:
                    self.node[BNF[operation]] = ''
                return True
            try:
                if self.keywords[self.tokens[self.h]] == operation.upper():
                    self.node[operation] = operation.upper()
                    self.token = operation.upper()
                    self.h += 1
                    return True
            except KeyError as e:
                #print 'Terminal key error ' + str(e)
                if operation in DELIMITERS:
                    if ord(operation) == self.tokens[self.h]:
                        self.node[operation] = operation
                        self.token = operation
                        self.h += 1
                        return True
                    return False
                if operation == 'identifier':
                    if self.tokens[self.h] > 1000:
                        if bnf:
                            self.node[operation] = self.identifiers[self.tokens[self.h]]
                        else:
                            self.node[BNF[operation]] = self.identifiers[self.tokens[self.h]]
                        self.token = self.identifiers[self.tokens[self.h]]
                        self.h += 1
                        return True
                if operation == 'constant':
                    if ((self.tokens[self.h]) < 1000 and
                        (self.tokens[self.h] > 500)):
                        if bnf:
                            self.node[operation] = self.constants[self.tokens[self.h]]
                        else:
                            self.node[BNF[operation]] = self.constants[self.tokens[self.h]]
                        self.token = self.constants[self.tokens[self.h]]
                        self.h += 1
                        return True
                return False


if __name__ == '__main__':
    attributes = dict_from_file('ascii.txt')
    keywords = dict_from_file('keywords.txt')

    scanner = Scanner(keywords, attributes)
    program = 'examples/valid_program.txt'
    #program = 'examples/comment.txt'
    #program = 'examples/invalid_char.txt'
    #program = 'examples/invalid_comment.txt'
    #program = 'examples/comment2.txt'
    tokens, keywords, constants, identifiers = scanner.run(program)
    keywords = reverse_dict(keywords)
    constants = reverse_dict(constants)
    identifiers = reverse_dict(identifiers)

    #print tokens
    parser = Parser(tokens, keywords, constants, identifiers)
    parser.start()
    #pprint.pprint(dicts(parser.tree))
    #print json.dumps(parser.tree, indent=4)